//
//  ChoosePhotoViewController.m
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import "ChoosePhotoViewController.h"

@interface ChoosePhotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UISwitch *switchAllowEditing;

@end

@implementation ChoosePhotoViewController {
    Boolean _isNewImagePicked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    if (self.classMate != nil && _isNewImagePicked == NO) {
        self.imgPhoto.image = self.classMate.photoImage;
    }
}

- (IBAction)btnPickImageFromLibraryPressed:(id)sender {
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = self.switchAllowEditing.isOn;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)btnTakeNewPhotoPressed:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
    } else {
        NSLog(@"NO CAMERA!");
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage* pickedImage;
    if(self.switchAllowEditing.isOn) {
        pickedImage = info[UIImagePickerControllerEditedImage];
    } else {
        pickedImage = info[UIImagePickerControllerOriginalImage];
    }
    self.imgPhoto.image = pickedImage;
    
    _isNewImagePicked = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnDonePressed:(id)sender {
    if (_isNewImagePicked) {
        NSString* imagePath = self.classMate.photoFilePath;
        NSData* imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 0.9); // 图片质量
        [imageData writeToFile:imagePath atomically:YES];
        NSLog(@"Image saved to:%@", imagePath);
        self.classMate.photo = [self.classMate photoFileName];
//        self.classMate.photoImage = [UIImage imageNamed:self.classMate.photo]
        [[ClassMateData sharedClassMateData] save];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
