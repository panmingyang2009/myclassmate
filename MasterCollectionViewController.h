//
//  MasterCollectionViewController.h
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterCollectionViewCell.h"
#import "ClassMateData.h"
#import "DetailTableViewController.h"

@interface MasterCollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@end
