//
//  DetailTableViewController.m
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import "DetailTableViewController.h"
#import "ChoosePhotoViewController.h"

@interface DetailTableViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCellphone;

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.classMate) {
        if (self.classMate.photo && self.classMate.photo.length > 0) {
            self.imgPhoto.image = self.classMate.photoImage;
        } else {
            self.imgPhoto.image = [UIImage imageNamed:@"no_image"];
        }
        if (self.classMate.name && self.classMate.name.length > 0) {
            self.lblName.text = self.classMate.name;
        } else {
            self.lblName.text = @"请输入姓名";
        }
        if (self.classMate.cellphone && self.classMate.cellphone.length > 0) {
            self.lblCellphone.text = self.classMate.cellphone;
        } else {
            self.lblCellphone.text = @"请输入手机";
        }
    } else {
        self.title = @"新建";
        self.imgPhoto.image = [UIImage imageNamed:@"no_image"];
        self.lblName.text = @"请输入姓名";
        self.lblCellphone.text = @"请输入手机";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.classMate) {
        if (self.classMate.photo && self.classMate.photo.length > 0) {
            self.imgPhoto.image = self.classMate.photoImage;
        } else {
            self.imgPhoto.image = [UIImage imageNamed:@"no_image"];
        }
        if (self.classMate.name && self.classMate.name.length > 0) {
            self.lblName.text = self.classMate.name;
        } else {
            self.lblName.text = @"请输入姓名";
        }
        if (self.classMate.cellphone && self.classMate.cellphone.length > 0) {
            self.lblCellphone.text = self.classMate.cellphone;
        } else {
            self.lblCellphone.text = @"请输入手机";
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self performSegueWithIdentifier:@"GoToPhoto" sender:self];
    } else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                [self performSegueWithIdentifier:@"GoToName" sender:self];
                break;
            case 1:
                [self performSegueWithIdentifier:@"GoToCellphone" sender:self];
                break;
                
            default:
                NSLog(@"DetailTVC: Error indexPath selected!");
                break;
        }
    }
}

- (IBAction)btnTrashPressed:(id)sender {
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"注意" message:@"确定删除此信息吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alertActionDelete = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        // 删除当前class mate
        [[ClassMateData sharedClassMateData] removeClassMate:self.classMate];
        [[ClassMateData sharedClassMateData] save];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction* alertActionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:alertActionDelete];
    [alertController addAction:alertActionCancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 2;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(section == 0)
//        return 0;
//    else
//        return 3;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"GoToPhoto"]) {
        ChoosePhotoViewController* chooseImage = segue.destinationViewController;
        if (self.classMate == nil) {
            // 新建
            self.classMate = [[ClassMateData sharedClassMateData] createClassMate];
        }
        chooseImage.classMate = self.classMate;
    } else if ([segue.identifier isEqualToString:@"GoToName"]) {
        ChangeMsgViewController* changeMsgVC = [segue destinationViewController];
        changeMsgVC.mode = ModeNameChange;
        changeMsgVC.classMate = self.classMate;
    } else if ([segue.identifier isEqualToString:@"GoToCellphone"]) {
        ChangeMsgViewController* changeMsgVC = [segue destinationViewController];
        changeMsgVC.mode = ModeCellphoneChange;
        changeMsgVC.classMate = self.classMate;
    }
}


@end
