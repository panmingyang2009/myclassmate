//
//  AppDelegate.h
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

