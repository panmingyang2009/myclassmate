//
//  ChangeMsgViewController.h
//  MyClassMate
//
//  Created by mac on 16/1/13.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassMateData.h"

typedef enum : NSUInteger {
    ModeNameChange,
    ModeCellphoneChange,
} Mode;

@interface ChangeMsgViewController : UIViewController

@property (nonatomic) ClassMate* classMate;
@property (nonatomic) Mode mode;

@end
