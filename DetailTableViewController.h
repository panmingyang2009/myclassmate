//
//  DetailTableViewController.h
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassMateData.h"
#import "ChangeMsgViewController.h"

@interface DetailTableViewController : UITableViewController

@property (nonatomic) ClassMate* classMate;

@end
