//
//  ChangeMsgViewController.m
//  MyClassMate
//
//  Created by mac on 16/1/13.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import "ChangeMsgViewController.h"

@interface ChangeMsgViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtInput;

@end

@implementation ChangeMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.mode == ModeNameChange) {
        self.title = @"请输入姓名";
        self.txtInput.placeholder = @"请输入姓名";
    } else if (self.mode == ModeCellphoneChange) {
        self.title = @"请输入手机号";
        self.txtInput.placeholder = @"请输入手机号";
    }
}

- (IBAction)btnConfigPressed:(id)sender {
    if (self.txtInput.text.length > 0) {
        if (self.mode == ModeNameChange) {
            self.classMate.name = self.txtInput.text;
        } else if (self.mode == ModeCellphoneChange) {
            self.classMate.cellphone = self.txtInput.text;
        } else {
            NSLog(@"ChangeMsgVC: wrong mode!");
        }
        
        // save changes
        [[ClassMateData sharedClassMateData] save];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
