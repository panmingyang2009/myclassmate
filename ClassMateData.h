//
//  ClassMateData.h
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ClassMate : NSObject

@property (nonatomic, readonly) NSString* uuid; //随机标识码, 创建时生成，无法修改
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* photo;
@property (nonatomic) NSString* cellphone;

- (instancetype)initWithDaia:(NSDictionary*)data;
- (NSDictionary*)toData;
- (UIImage*)photoImage;
- (NSString*)photoFilePath;
- (NSString*)photoFileName;

@end


@interface ClassMateData : NSObject

@property (nonatomic) NSMutableArray<ClassMate*>* classMates;

+ (ClassMateData*)sharedClassMateData;

- (void)save;
- (ClassMate*)createClassMate;
- (void)removeClassMate:(ClassMate*)classMate;

@end
