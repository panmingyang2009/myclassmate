//
//  ClassMateData.m
//  MyClassMate
//
//  Created by mac on 16/1/12.
//  Copyright © 2016年 lanqiao. All rights reserved.
//

#import "ClassMateData.h"
#import <UIKit/UIKit.h> // import才能使用UIImage

@implementation ClassMate

- (instancetype)init
{
    self = [super init];
    if (self) {
        _uuid = [[NSUUID UUID] UUIDString];
    }
    return self;
}

- (instancetype)initWithDaia:(NSDictionary *)data {
    self = [super init];
    if (self) {
        _uuid = data[@"uuid"];
        _name = data[@"name"];
        _photo = data[@"photo"];
        _cellphone = data[@"cellphone"];
    }
    return self;
}

- (NSDictionary *)toData {
    NSMutableDictionary* data = [[NSMutableDictionary alloc] initWithCapacity:4];
    data[@"uuid"] = _uuid;
    data[@"name"] = _name;
    data[@"photo"] = _photo;
    data[@"cellphone"] = _cellphone;
    return data;
}

- (UIImage *)photoImage {
    if (self.photo != nil && self.photo.length > 0) {
        NSData* imageData = [NSData dataWithContentsOfFile:[self photoFilePath]];
        return [UIImage imageWithData:imageData];
//        return [UIImage imageNamed:[self photoFilePath]];
    } else {
        return [UIImage imageNamed:@"no_image"];
    }
}

// 拼接文件名
- (NSString*)photoFileName {
    return [NSString stringWithFormat:@"%@.jpg", _uuid];
}

// 拼接路径+文件名
- (NSString*)photoFilePath {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths firstObject] stringByAppendingPathComponent:[self photoFileName]];
}

@end


@implementation ClassMateData

+ (ClassMateData *)sharedClassMateData {
    static ClassMateData* sharedClassMateData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClassMateData = [[self alloc] init];
    });
    return sharedClassMateData;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _classMates = [NSMutableArray arrayWithCapacity:29];
        
        NSString* dataFilePath = [self dataFilePath];
        NSLog(@"Class Mate Data: Loading data from %@", dataFilePath);
        
        NSArray* classMateArray = [NSArray arrayWithContentsOfFile:dataFilePath];
        
        if (classMateArray ==nil || classMateArray.count < 1) {
            NSLog(@"Class Mate Data: Data file in app documents folder doesn't exist or is empty, reloading default plist from bundle");
            classMateArray = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ClassMates" ofType:@"plist"]];
        }
        for (NSDictionary* data in classMateArray) {
            ClassMate* classMate = [[ClassMate alloc] initWithDaia:data];
            [_classMates addObject:classMate];
        }
    }
    return self;
}

- (ClassMate *)createClassMate {
    ClassMate* classMate = [[ClassMate alloc] init];
    [_classMates addObject:classMate];
    return classMate;
}

- (void)removeClassMate:(ClassMate*)classMate {
    NSLog(@"Removing class mate from list.");
    [_classMates removeObject:classMate];
    if (classMate.photo != nil && classMate.photo.length > 0) {
        NSError* error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[classMate photoFilePath] error:&error];
    }
}

- (void)save {
    NSMutableArray* classMatesArray = [[NSMutableArray alloc] initWithCapacity:29];
    for (ClassMate* classMate in _classMates) {
        [classMatesArray addObject:[classMate toData]];
    }
    [classMatesArray writeToFile:[self dataFilePath] atomically:YES];
}

- (NSString*)dataFilePath {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths firstObject] stringByAppendingPathComponent:@"ClassMatas.plist"];
}

@end
